import { observer } from "mobx-react-lite";
import { PRECISION } from "../../config/consts";
import { roundToPrecision } from "../../utils/general";
import { LogBox, StyledDate, Title } from "./styled";

interface ExchangeLogProps {
  log: ExchangeLog[];
}

const ExchangeLog: React.FC<ExchangeLogProps> = ({ log }) => {
  if (log.length === 0) {
    return null;
  }
  return (
    <>
      <Title>Exchange History:</Title>
      {log.map((el) => (
        <LogBox key={el.date}>
          <p>
            From {el.from.code} (
            <strong>-{roundToPrecision(el.from.value, PRECISION)}</strong>) to{" "}
            {el.to.code} (
            <strong>+{roundToPrecision(el.to.value, PRECISION)}</strong>)
          </p>
          <StyledDate>{new Date(el.date).toLocaleString()}</StyledDate>
        </LogBox>
      ))}
    </>
  );
};

export default observer(ExchangeLog);
