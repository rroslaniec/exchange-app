import ExchangeLog from ".";
import { render } from "../../test-utils";

describe("ExchangeLog", () => {
  const fakeData: ExchangeLog[] = [
    {
      date: new Date("2021-02-07").toISOString(),
      from: {
        code: "FOO",
        value: 1.1234,
      },
      to: {
        code: "BAR",
        value: 1.654,
      },
    },
  ];
  it("should not render when lenght of log is 0", async () => {
    const screen = render(<ExchangeLog log={[]} />);
    const element = await screen.queryByText("Exchange History:");
    expect(element).not.toBeInTheDocument();
  });

  it("should render elements with user friendly values", () => {
    const screen = render(<ExchangeLog log={fakeData} />);
    expect(screen.getByText(/1\.12/)).toBeInTheDocument();
  });
});
