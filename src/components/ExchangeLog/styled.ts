import styled from "styled-components";

export const LogBox = styled.div`
  padding: 10px;
  display: flex;
  flex-direction: column;
  & + & {
    border-top: 1px solid;
  }
`;

export const StyledDate = styled.p`
  font-size: ${({ theme }) => theme.sizes.small};
  font-weight: 300;
`;

export const Title = styled.p`
  font-size: ${({ theme }) => theme.sizes.large};
  font-weight: 700;
  text-align: center;
  color: ${({ theme }) => theme.colors.secondary};
`;
