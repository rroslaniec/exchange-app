import { observer } from "mobx-react-lite";
import React, { useCallback } from "react";
import useCurrencyStore from "../../hooks/useCurrencyStore";
import useUserStore from "../../hooks/useUserStore";
import { StyledSelect } from "./styled";

export const SELECT_ID = "currency-select";

const CurrencySelect = () => {
  const { exchangeTo, setExchangeCurrency } = useUserStore((store) => ({
    exchangeTo: store.exchangeTo,
    setExchangeCurrency: store.setExchangeCurrency,
  }));
  const { currenciesWithRates } = useCurrencyStore((store) => ({
    currenciesWithRates: store.currenciesWithRates,
  }));

  const handleChangeExchangeCurrency: React.ChangeEventHandler<HTMLSelectElement> =
    useCallback(
      (e) => {
        setExchangeCurrency(e.target.value);
      },
      [setExchangeCurrency]
    );

  return (
    <>
      <label htmlFor={SELECT_ID}>
        Pick currency to exchange:
        <StyledSelect
          id={SELECT_ID}
          data-testid={SELECT_ID}
          value={exchangeTo}
          onChange={handleChangeExchangeCurrency}
        >
          {Object.keys(currenciesWithRates).map((key) => (
            <option key={key} value={key}>
              {currenciesWithRates[key]}
            </option>
          ))}
        </StyledSelect>
      </label>
    </>
  );
};

export default observer(CurrencySelect);
