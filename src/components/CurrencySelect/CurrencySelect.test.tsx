import userEvent from "@testing-library/user-event";
import { render } from "../../test-utils";
import CurrencySelect from ".";
import useCurrencyStore from "../../hooks/useCurrencyStore";
import useUserStore from "../../hooks/useUserStore";
import { SELECT_ID } from "./CurrencySelect";

jest.mock("../../hooks/useCurrencyStore");
jest.mock("../../hooks/useUserStore");

describe("CurrencySelect", () => {
  it("should render correct amount of options", async () => {
    (useCurrencyStore as jest.Mock).mockReturnValueOnce({
      currenciesWithRates: { test: "test", test1: "test" },
    });
    (useUserStore as jest.Mock).mockReturnValueOnce({
      exchangeTo: "PLN",
      setExchangeCurrency: jest.fn(),
    });

    const screen = render(<CurrencySelect />);
    const options = await screen.findAllByText("test");
    expect(options).toHaveLength(2);
  });

  it("should pass picked by user currency to store", async () => {
    const fn = jest.fn();
    (useCurrencyStore as jest.Mock).mockReturnValueOnce({
      currenciesWithRates: { test: "1", foo: "bar" },
    });
    (useUserStore as jest.Mock).mockReturnValueOnce({
      exchangeTo: "PLN",
      setExchangeCurrency: fn,
    });

    const screen = render(<CurrencySelect />);
    userEvent.selectOptions(screen.getByTestId(SELECT_ID), ["bar"]);
    expect(fn).toBeCalled();
    expect(fn).toBeCalledWith("foo");
  });
});
