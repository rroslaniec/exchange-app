import styled from "styled-components";

export const StyledSelect = styled.select`
  padding: 10px;
  font-size: ${({ theme }) => theme.sizes.medium};
  outline: none;
  background-color: ${({ theme }) => theme.colors.secondary};
  border-radius: 5px;
  margin: 10px;
`;
