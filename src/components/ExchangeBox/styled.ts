import styled from "styled-components";
import RateInfo from "../RateInfo";

export const BoxWrapper = styled.label`
  padding: 30px;
  display: flex;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.secondary};
  margin: 10px;
  border-radius: 5px;
  position: relative;

  @media (min-width: 768px) {
    max-width: 50%;
  }
  @media (min-width: 1280px) {
    width: 30%;
  }
`;

export const WalletInfo = styled.span`
  font-size: ${({ theme }) => theme.sizes.small};
  font-style: italic;
`;

export const StyledRateInfo = styled(RateInfo)`
  font-size: ${({ theme }) => theme.sizes.small};
  font-style: italic;
  position: absolute;
  right: 10px;
  bottom: 0px;
`;
