import { fireEvent } from "@testing-library/react";
import ExchangeBox from ".";
import useUserStore from "../../hooks/useUserStore";
import useCurrencyStore from "../../hooks/useCurrencyStore";
import { render } from "../../test-utils";
import { TEST_ID } from "./ExchangeBox";

jest.mock("../../hooks/useUserStore");
jest.mock("../../hooks/useCurrencyStore");
describe("ExchangeBox", () => {
  const currency = "FOO";
  const fakeData = {
    value: 1,
    inWallet: 100,
    set: jest.fn(),
  };

  it("should not render Box when there is no data", async () => {
    (useUserStore as jest.Mock).mockReturnValueOnce({
      getDataAboutCurrency: () => null,
    });
    const screen = render(<ExchangeBox currency={currency} />);
    const element = await screen.queryByText(currency);
    expect(element).not.toBeInTheDocument();
  });

  it("should render correct header according to perfix", () => {
    (useUserStore as jest.Mock).mockReturnValueOnce({
      getDataAboutCurrency: () => fakeData,
    });
    let screen = render(<ExchangeBox currency={currency} />);
    expect(screen.getByText("From")).toBeInTheDocument();
    (useUserStore as jest.Mock).mockReturnValueOnce({
      getDataAboutCurrency: () => fakeData,
    });
    screen = render(<ExchangeBox currency={currency} prefix="+" />);
    expect(screen.getByText("To")).toBeInTheDocument();
  });

  it("should display user friendly wallet balance for specific currency", () => {
    (useUserStore as jest.Mock).mockReturnValueOnce({
      getDataAboutCurrency: () => ({ ...fakeData, inWallet: 11.121 }),
    });
    const screen = render(<ExchangeBox currency={currency} />);
    expect(screen.getByText("11.12")).toBeInTheDocument();
  });
  it("should fire onChange fn while types", () => {
    const handler = jest.fn();
    (useUserStore as jest.Mock).mockReturnValueOnce({
      getDataAboutCurrency: () => ({ ...fakeData, set: handler }),
    });
    const screen = render(<ExchangeBox currency={currency} />);
    const element = screen.getByTestId(currency);
    fireEvent.change(element, { target: { value: "11" } });

    expect(handler).toBeCalled();
  });
  it("should display rate for '+' prefix", () => {
    (useUserStore as jest.Mock).mockReturnValueOnce({
      getDataAboutCurrency: () => fakeData,
    });
    (useUserStore as jest.Mock).mockReturnValueOnce({
      getDataAboutCurrency: () => fakeData,
    });
    (useCurrencyStore as jest.Mock).mockReturnValueOnce(1.23);
    const screen = render(<ExchangeBox currency="PLN" prefix="+" />);
    expect(screen.getByText("1 PLN = 1.23")).toBeInTheDocument();
  });

  it("should whole wrapper have for attr", () => {
    (useUserStore as jest.Mock).mockReturnValueOnce({
      getDataAboutCurrency: () => fakeData,
    });
    const screen = render(<ExchangeBox currency={currency} />);
    const element = screen.getByTestId(TEST_ID);
    expect(element).toHaveProperty("htmlFor", currency);
  });
});
