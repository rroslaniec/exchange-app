import { fireEvent } from "@testing-library/dom";
import CurrencyInput from ".";
import { render } from "../../../test-utils";

describe("CurrencyInput", () => {
  const handler = jest.fn();

  afterEach(() => {
    handler.mockClear();
  });

  it("should not render prefix if value is 0", async () => {
    const screen = render(
      <CurrencyInput id="foo" value={0} onChange={handler} />
    );
    const element = await screen.queryByText("-");
    expect(element).not.toBeInTheDocument();
  });

  it("should render prefix if value is not 0", () => {
    const screen = render(
      <CurrencyInput id="foo" value={1} onChange={handler} />
    );
    const element = screen.getByText("-");
    expect(element).toBeInTheDocument();
  });

  it("should autofocus for '-' prefix", () => {
    const screen = render(
      <CurrencyInput id="foo" value={1} onChange={handler} prefix="-" />
    );
    const element = screen.getByTestId("foo");
    expect(element).toHaveFocus();
  });

  it("should not autofocus for '+' prefix", () => {
    const screen = render(
      <CurrencyInput id="foo" value={1} onChange={handler} prefix="+" />
    );
    const element = screen.getByTestId("foo");
    expect(element).not.toHaveFocus();
  });

  it("should display user friendly value", () => {
    const screen = render(
      <CurrencyInput id="foo" value={1.119} onChange={handler} prefix="+" />
    );
    const element = screen.getByTestId("foo");
    expect(element).toHaveValue(1.12);
  });
  it("should fire onChange fn while types", () => {
    const screen = render(
      <CurrencyInput id="foo" value={1.119} onChange={handler} prefix="+" />
    );
    const element = screen.getByTestId("foo");
    fireEvent.change(element, { target: { value: "1" } });

    expect(handler).toBeCalled();
  });
});
