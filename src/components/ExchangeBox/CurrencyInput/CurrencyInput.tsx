import { observer } from "mobx-react-lite";
import { PRECISION } from "../../../config/consts";
import { roundToPrecision } from "../../../utils/general";
import { StyledInput, Wrapper } from "./styled";

interface CurrencyInputProps {
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  value?: number;
  prefix?: ExchangePrefix;
  id: string;
}

const CurrencyInput: React.FC<CurrencyInputProps> = ({
  value,
  onChange,
  id,
  prefix = "-",
}) => {
  const rounded = roundToPrecision(value, PRECISION);
  return (
    <Wrapper>
      {value ? prefix : null}
      <StyledInput
        autoFocus={prefix === "-"}
        type="number"
        id={id}
        data-testid={id}
        value={rounded || ""}
        onChange={onChange}
      />
    </Wrapper>
  );
};

export default observer(CurrencyInput);
