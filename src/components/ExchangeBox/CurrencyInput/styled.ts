import styled from "styled-components";

export const StyledInput = styled.input`
  background: none;
  border: none;
  outline: none;
  font-size: ${({ theme }) => theme.sizes.xlarge};
  width: 150px;
`;

export const Wrapper = styled.div`
  font-size: ${({ theme }) => theme.sizes.xlarge};
  margin-left: auto;
`;
