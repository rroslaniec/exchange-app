import { observer } from "mobx-react-lite";
import React, { useCallback } from "react";
import { PRECISION } from "../../config/consts";
import useUserStore from "../../hooks/useUserStore";
import { roundToPrecision } from "../../utils/general";
import CodeLabel from "../CodeLabel";
import CurrencyInput from "./CurrencyInput";
import { BoxWrapper, StyledRateInfo, WalletInfo } from "./styled";

export const TEST_ID = "box-wrapper";

interface ExchangeBoxProps {
  currency: string;
  prefix?: ExchangePrefix;
}

const ExchangeBox: React.FC<ExchangeBoxProps> = ({
  currency,
  prefix = "-",
}) => {
  const { getDataAboutCurrency } = useUserStore((store) => ({
    getDataAboutCurrency: store.getDataAboutCurrency,
  }));
  const data = getDataAboutCurrency(currency);

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = useCallback(
    (e) => {
      data?.set(+e.target.value);
    },
    [data]
  );
  if (!data) return null;

  return (
    <>
      <p>
        <strong>{prefix === "-" ? "From" : "To"}</strong>
      </p>
      <BoxWrapper htmlFor={currency} data-testid={TEST_ID}>
        <CodeLabel>{currency}</CodeLabel>
        <WalletInfo>
          You have:
          <strong>{roundToPrecision(data.inWallet, PRECISION)}</strong>
        </WalletInfo>
        <CurrencyInput
          id={currency}
          value={data.value}
          onChange={handleChange}
          prefix={prefix}
        />
        {prefix === "+" && <StyledRateInfo currency={currency} />}
      </BoxWrapper>
    </>
  );
};

export default observer(ExchangeBox);
