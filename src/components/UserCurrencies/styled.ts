import styled from "styled-components";

export const WalletItems = styled.div`
  display: flex;
  flex-wrap: wrap;
  padding: 10px;
  justify-content: center;
  align-items: center;
`;
