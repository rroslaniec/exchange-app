import UserCurrencies from ".";
import { render } from "../../test-utils";

describe("UserCurrencies", () => {
  const fakeWallet: Wallet = new Map([["PLN", 1]]);
  it("should render items only those in the wallet", async () => {
    const screen = render(<UserCurrencies wallet={fakeWallet} />);
    const element = await screen.queryByText("Polish Zloty");
    const otherElement = await screen.queryByText("Euro");

    expect(element).toBeInTheDocument();
    expect(otherElement).not.toBeInTheDocument();
  });
});
