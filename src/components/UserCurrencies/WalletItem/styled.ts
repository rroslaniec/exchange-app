import { Link } from "react-router-dom";
import styled from "styled-components";

export const Wrapper = styled(Link)`
  position: relative;
  border-radius: 5px;
  border: 2px solid;
  padding: 10px;
  width: 150px;
  height: 150px;
  margin: 10px;
  background-color: ${({ theme }) => theme.colors.background};
  color: ${({ theme }) => theme.colors.secondary};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  transition: box-shadow 0.3s ease-in-out;
  text-decoration: none;
  text-align: center;
  font-size: ${({ theme }) => theme.sizes.medium};

  &:hover {
    box-shadow: 0px 0px 9px 0px #000;
  }

  @media (max-width: 768px) {
    width: 120px;
    height: 120px;
    font-size: ${({ theme }) => theme.sizes.small};
  }
`;

export const Value = styled.div`
  color: ${({ theme }) => theme.colors.primary};
  font-size: ${({ theme }) => theme.sizes.xlarge};
  font-weight: 700;
  padding: 20px;
  @media (max-width: 768px) {
    font-size: ${({ theme }) => theme.sizes.large};
  }
`;
