import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { PRECISION } from "../../../config/consts";
import useUserStore from "../../../hooks/useUserStore";
import { roundToPrecision } from "../../../utils/general";
import CodeLabel from "../../CodeLabel";
import { Value, Wrapper } from "./styled";

export const TEST_ID = "WalletItem";

interface WalletItemProps {
  code: string;
  name: string;
  value: number;
}

const WalletItem: React.FC<WalletItemProps> = ({ code, name, value }) => {
  const setCurrency = useUserStore((store) => store.setCurrency);
  const handleClick = useCallback(() => {
    setCurrency(code);
  }, [code, setCurrency]);

  if (!value) return null;

  return (
    <Wrapper to="/exchange" onClick={handleClick} data-testid={TEST_ID}>
      <CodeLabel>{code}</CodeLabel>
      <Value>{roundToPrecision(value, PRECISION)}</Value>
      {name}
    </Wrapper>
  );
};

export default observer(WalletItem);
