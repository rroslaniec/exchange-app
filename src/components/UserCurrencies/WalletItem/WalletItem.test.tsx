import userEvent from "@testing-library/user-event";
import WalletItem from ".";
import { render } from "../../../test-utils";
import useUserStore from "../../../hooks/useUserStore";
import { TEST_ID } from "./WalletItem";

jest.mock("../../../hooks/useUserStore");

describe("WalletItem", () => {
  const handler = jest.fn();
  it("should not render when wallet is empty for specified currency", async () => {
    const screen = render(
      <WalletItem value={0} code="PLN" name="Polski Złoty" />
    );
    const element = await screen.queryByText("Polski Złoty");
    expect(element).not.toBeInTheDocument();
  });
  it("should render name of currency", () => {
    const screen = render(
      <WalletItem value={1} code="PLN" name="Polski Złoty" />
    );
    expect(screen.getByText("Polski Złoty")).toBeInTheDocument();
  });
  it("should trigger handler and redirect to exchange view", () => {
    (useUserStore as jest.Mock).mockReturnValueOnce(handler);
    const screen = render(
      <WalletItem value={1.123} code="PLN" name="Polski Złoty" />
    );
    const element = screen.getByTestId(TEST_ID);
    userEvent.click(element);
    expect(handler).toBeCalledWith("PLN");
    expect(element).toHaveProperty("href", "http://localhost/exchange");
  });
});
