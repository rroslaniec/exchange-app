import { observer } from "mobx-react-lite";
import useCurrencyStore from "../../hooks/useCurrencyStore";
import { WalletItems } from "./styled";
import WalletItem from "./WalletItem";

interface UserCurrenciesProps {
  wallet: Wallet;
}

const UserCurrencies: React.FC<UserCurrenciesProps> = ({ wallet }) => {
  const currencies = useCurrencyStore((store) => store.currencies);
  return (
    <WalletItems>
      {Array.from(wallet.keys()).map(
        (code) =>
          wallet.has(code) && (
            <WalletItem
              key={code}
              code={code}
              value={wallet.get(code)}
              name={currencies[code]}
            />
          )
      )}
    </WalletItems>
  );
};

export default observer(UserCurrencies);
