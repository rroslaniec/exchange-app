import { observer } from "mobx-react-lite";
import React from "react";
import useCurrencyStore from "../hooks/useCurrencyStore";
import useUserStore from "../hooks/useUserStore";

export const TEST_ID = "RateInfo";

interface RateInfoProps {
  currency: string;
  className?: string;
}

const RateInfo: React.FC<RateInfoProps> = ({ currency, className }) => {
  const data = useUserStore((store) => store.getRateInfoByCurrency(currency));

  const rate = useCurrencyStore((store) => {
    if (!data?.baseCurrency) return null;
    return store[data?.ratesName][data?.baseCurrency];
  });

  if (!rate || !data) return null;
  return (
    <p className={className} data-testid={TEST_ID}>
      1 {currency} = {rate} {data.baseCurrency}
    </p>
  );
};

export default observer(RateInfo);
