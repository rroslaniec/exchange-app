import { render } from "../test-utils";
import RateInfo, { TEST_ID } from "./RateInfo";
import useUserStore from "../hooks/useUserStore";
import useCurrencyStore from "../hooks/useCurrencyStore";

jest.mock("../hooks/useUserStore");
jest.mock("../hooks/useCurrencyStore");

describe("RateInfo", () => {
  const fakeUserData = { baseCurrency: "PLN", ratesName: "fromRates" };
  it("should not render rates when passed currency is not in store", async () => {
    (useUserStore as jest.Mock).mockReturnValueOnce(null);
    (useCurrencyStore as jest.Mock).mockReturnValueOnce(1.23);

    const screen = render(<RateInfo currency="PLN" />);
    const element = await screen.queryByText(/=/);
    expect(element).not.toBeInTheDocument();
  });

  it("should not render rates when currency is not in store", async () => {
    (useUserStore as jest.Mock).mockReturnValueOnce({
      ...fakeUserData,
      baseCurrency: null,
    });

    const screen = render(<RateInfo currency="PLN" />);
    const element = await screen.queryByText(/=/);
    expect(element).not.toBeInTheDocument();
  });

  it("should render rate for currency", () => {
    (useUserStore as jest.Mock).mockReturnValueOnce(fakeUserData);
    (useCurrencyStore as jest.Mock).mockReturnValueOnce(1.23);

    const screen = render(<RateInfo currency="PLN" />);
    expect(screen.getByText(/1.23/)).toBeInTheDocument();
  });
  it("should apply className", () => {
    (useUserStore as jest.Mock).mockReturnValueOnce(fakeUserData);
    (useCurrencyStore as jest.Mock).mockReturnValueOnce(1.23);

    const screen = render(<RateInfo currency="PLN" className="foo" />);
    expect(screen.getByTestId(TEST_ID)).toHaveClass("foo");
  });
});
