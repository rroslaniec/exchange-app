import styled from "styled-components";

const CodeLabel = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  padding: 5px;
  font-weight: 700;
`;

export default CodeLabel;
