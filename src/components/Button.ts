import styled from "styled-components";

const Button = styled.button`
  padding: 10px;
  background-color: ${({ theme }) => theme.colors.primary};
  color: ${({ theme }) => theme.colors.background};
  border-radius: 5px;
  font-weight: 700;
  font-size: ${({ theme }) => theme.sizes.large};
`;

export default Button;
