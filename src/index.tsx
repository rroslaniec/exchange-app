import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { createGlobalStyle, ThemeProvider } from "styled-components";
import App from "./App";
import { THEME } from "./config/theme";
import { GlobalStoreProvider } from "./contexts/GlobalContext";

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
    'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    background-color: #3F826D;
    padding: 10px;
  }
  * {
    box-sizing: border-box;
  }
`;

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={THEME}>
      <GlobalStoreProvider>
        <Router>
          <>
            <App />
            <GlobalStyle />
          </>
        </Router>
      </GlobalStoreProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
