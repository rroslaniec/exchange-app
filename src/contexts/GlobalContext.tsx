import { useLocalObservable } from "mobx-react-lite";
import { createContext, useContext } from "react";
import RootStore from "../stores";

const GlobalStoreContext = createContext<typeof RootStore | null>(null);

export const useRootStore = () => useContext(GlobalStoreContext);

export const GlobalStoreProvider: React.FC = ({ children }) => {
  const store = useLocalObservable(() => RootStore);

  return (
    <GlobalStoreContext.Provider value={store}>
      {children}
    </GlobalStoreContext.Provider>
  );
};

export default GlobalStoreContext;
