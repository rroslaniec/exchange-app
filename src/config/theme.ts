import { DefaultTheme } from "styled-components";

/* eslint-disable import/prefer-default-export */
export const THEME: DefaultTheme = {
  colors: {
    primary: "#F7F7FF",
    secondary: "#F2D0A4",
    background: "#545E75",
  },
  sizes: {
    small: "12px",
    medium: "15px",
    large: "18px",
    xlarge: "25px",
  },
};
