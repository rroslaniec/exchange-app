export const RATES_UPDATE_TIME = 10_000;

export enum LocalStorageKeys {
  EXCHANGE_LOG = "EXCHANGE_LOG",
  WALLET = "WALLET",
}

// Prevents log to blow-up localstorge
export const EXCHANGE_LOG_HISTORY = 100;

// Global round precision
export const PRECISION = 2;
