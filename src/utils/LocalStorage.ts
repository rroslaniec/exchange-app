class LocalStorage {
  static getJSON(key: string) {
    try {
      const data = window.localStorage.getItem(key);
      const parsedData = data ? JSON.parse(data) : null;
      if (typeof parsedData !== "object") {
        return null;
      }
      return parsedData;
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  static saveJSON(key: string, value: unknown) {
    try {
      window.localStorage.setItem(key, JSON.stringify(value));
    } catch (error) {
      console.error(error);
    }
  }

  static hasItem(key: string) {
    return !!window.localStorage.getItem(key);
  }
}

export default LocalStorage;
