import { roundToPrecision } from "./general";

describe("general utils", () => {
  describe("roundToPrecision", () => {
    it("should correctly round to precision", () => {
      expect(roundToPrecision(0.111, 2)).toEqual(0.11);
    });
    it("should return undefine for undefined value", () => {
      expect(roundToPrecision(undefined, 2)).toEqual(undefined);
    });
  });
});
