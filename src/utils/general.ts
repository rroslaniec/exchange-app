/* eslint-disable import/prefer-default-export */
export const roundToPrecision = (
  value: number | undefined,
  precision: number
) => {
  if (value === undefined) return value;
  return parseFloat(value.toFixed(precision));
};
