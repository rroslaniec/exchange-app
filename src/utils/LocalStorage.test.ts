import LocalStorage from "./LocalStorage";

const mockWindowProperty = (property: any, value: any) => {
  const { [property]: originalProperty } = window;
  delete window[property];
  beforeAll(() => {
    Object.defineProperty(window, property, {
      configurable: true,
      writable: true,
      value,
    });
  });
  afterAll(() => {
    window[property] = originalProperty;
  });
};

describe("LocalStorage", () => {
  const mockedGetItem = jest.fn();
  const mockedSetItem = jest.fn();

  mockWindowProperty("localStorage", {
    getItem: mockedGetItem,
    setItem: mockedSetItem,
  });

  afterEach(() => {
    mockedGetItem.mockReset();
    mockedSetItem.mockReset();
  });

  it("should check for item", () => {
    LocalStorage.hasItem("foo");
    expect(window.localStorage.getItem).toHaveBeenCalled();
  });

  it("should save object as string", () => {
    const data = { foo: "bar" };
    LocalStorage.saveJSON("key", data);
    expect(window.localStorage.setItem).toHaveBeenCalledWith(
      "key",
      JSON.stringify(data)
    );
  });

  it("should handle error when tries to save not serializable object", () => {
    const circularReference: any = { otherData: 123 };
    circularReference.myself = circularReference;
    LocalStorage.saveJSON("key", circularReference);
    expect(window.localStorage.setItem).not.toHaveBeenCalled();
  });

  it("should get data from storage and parse it", () => {
    mockedGetItem.mockReturnValueOnce('{"foo":"bar"}');

    expect(LocalStorage.getJSON("key")).toEqual({ foo: "bar" });
  });

  it("should get only objets from storage and parse it", () => {
    mockedGetItem.mockReturnValueOnce("0");

    expect(LocalStorage.getJSON("key")).toEqual(null);
  });

  it("should handle problems with corrupted data", () => {
    mockedGetItem.mockReturnValueOnce('{"foo":"bar}');

    expect(LocalStorage.getJSON("key")).toEqual(null);
  });
});
