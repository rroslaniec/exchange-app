import BaseAPI from "./BaseAPI";

class CurrenciesAPI extends BaseAPI {
  baseUrl = "https://openexchangerates.org/api";

  fetchCurrencies(): Promise<CurrenciesResponse> {
    return this.fetch<CurrenciesResponse>("/currencies.json");
  }
}

export default CurrenciesAPI;
