import BaseAPI from "./BaseAPI";

class RatesAPI extends BaseAPI {
  fetchRates(base: string): Promise<RatesResponse> {
    return this.fetch<RatesResponse>(`/latest?base=${base}`);
  }
}

export default RatesAPI;
