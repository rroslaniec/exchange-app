import rates from "./rates.json";

class RatesAPI {
  fetchRates = () => Promise.resolve(rates);
}

export default RatesAPI;
