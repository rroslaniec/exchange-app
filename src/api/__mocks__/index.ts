import CurrenciesAPI from "./CurrenciesAPI";
import RatesAPI from "./RatesAPI";

const API = {
  Currencies: new CurrenciesAPI(),
  Rates: new RatesAPI(),
};

export default API;
