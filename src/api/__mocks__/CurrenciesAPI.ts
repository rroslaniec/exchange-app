import currencies from "./currencies.json";

class CurrenciesAPI {
  fetchCurrencies = () => Promise.resolve(currencies);
}

export default CurrenciesAPI;
