class BaseAPI {
  baseUrl = "https://api.ratesapi.io/api";

  async fetch<T>(path: string, init?: RequestInit): Promise<T> {
    const response = await fetch(this.baseUrl + path, init);

    if (!response.ok) {
      return Promise.reject(new Error(await response.text()));
    }
    try {
      const data: T = await response.json();
      return data;
    } catch (error) {
      return Promise.reject(error);
    }
  }
}

export default BaseAPI;
