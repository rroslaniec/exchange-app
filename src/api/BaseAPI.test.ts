import fetchMock from "jest-fetch-mock";
import BaseAPI from "./BaseAPI";

describe("BaseAPI", () => {
  const baseApiInstance = new BaseAPI();
  fetchMock.enableMocks();
  afterAll(() => {
    fetchMock.resetMocks();
  });

  it("should throw when response is not ok", async () => {
    fetchMock.mockRejectOnce(new Error("foo"));
    const response = baseApiInstance.fetch("/test/");
    await expect(response).rejects.toBeTruthy();
  });
  it("should return parsed data for response", async () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    fetchMock.mockResolvedValueOnce({
      json: () => Promise.resolve({ body: "test" }),
      ok: true,
    });
    expect(await baseApiInstance.fetch("/test/")).toEqual({
      body: "test",
    });
  });

  it("should throw for problems with json", async () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    fetchMock.mockResolvedValueOnce({
      json: () => {
        throw Error("foo");
      },
      ok: true,
    });
    const response = baseApiInstance.fetch("/test/");
    await expect(response).rejects.toBeTruthy();
  });
});
