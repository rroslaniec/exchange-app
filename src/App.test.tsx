import userEvent from "@testing-library/user-event";
import { render } from "./test-utils";

import App from "./App";

describe("App", () => {
  it("should render home route by default", () => {
    const screen = render(<App />);
    expect(screen.getByText("Your wallet")).toBeInTheDocument();
  });
  it("should render wallet route after user clicks for exchange currency", () => {
    const screen = render(<App />);
    userEvent.click(screen.getByText(/USD/i));
    expect(screen.getByText("exchange")).toBeInTheDocument();
  });
});
