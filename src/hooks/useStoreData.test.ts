import React from "react";
import useStoreData from "./useStoreData";

jest.mock("react");

describe("useStoreData", () => {
  it("should raise error when there is no store context", () => {
    expect(() =>
      useStoreData(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        null,
        () => {},
        () => {}
      )
    ).toThrow();
  });
  it("should pass correct value from store", () => {
    const context = {
      FooStore: {
        bar: "test",
      },
    };
    (React.useContext as jest.Mock).mockReturnValueOnce(context);
    expect(
      useStoreData<string, typeof context, typeof context["FooStore"]>(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        null,
        (store) => store.FooStore,
        (store) => store.bar
      )
    ).toEqual("test");
  });
});
