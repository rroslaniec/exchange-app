import GlobalStoreContext from "../contexts/GlobalContext";
import CurrencyStore from "../stores/CurrencyStore";
import useStoreData from "./useStoreData";

const useCurrencyStore = <Selection>(
  dataSelector: (store: CurrencyStore) => Selection
) =>
  useStoreData(
    GlobalStoreContext,
    (contextData) => contextData!.currencyStore!,
    dataSelector
  );

export default useCurrencyStore;
