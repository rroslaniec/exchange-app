import GlobalStoreContext from "../contexts/GlobalContext";
import UserStore from "../stores/UserStore";
import useStoreData from "./useStoreData";

const useUserStore = <Selection>(
  dataSelector: (store: UserStore) => Selection
) =>
  useStoreData(
    GlobalStoreContext,
    (contextData) => contextData!.userStore!,
    dataSelector
  );

export default useUserStore;
