import GlobalStoreContext from "../contexts/GlobalContext";
import rootStore from "../stores";
import useStoreData from "./useStoreData";

const useGlobalStore = <Selection>(
  dataSelector: (store: typeof rootStore) => Selection
) =>
  useStoreData(GlobalStoreContext, (contextData) => contextData!, dataSelector);

export default useGlobalStore;
