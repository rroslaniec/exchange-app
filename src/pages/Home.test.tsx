import Home from "./Home";
import { render } from "../test-utils";

describe("Home Page", () => {
  it("should apply correct page title", () => {
    render(<Home />);
    expect(document.title).toContain("Wallet");
  });
});
