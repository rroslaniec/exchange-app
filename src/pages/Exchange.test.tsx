import Exchange from "./Exchange";
import { render } from "../test-utils";
import rootStore from "../stores";

describe("Exchange Page", () => {
  beforeEach(() => {
    rootStore.userStore.setExchangeCurrency(undefined);
    rootStore.userStore.setCurrency(undefined);
  });
  afterEach(() => {
    rootStore.userStore.setExchangeCurrency(undefined);
    rootStore.userStore.setCurrency(undefined);
  });

  it("should redirect if there is no exchangeFrom and/or exchangeTo in store", async () => {
    rootStore.userStore.setCurrency("PLN");
    rootStore.userStore.setExchangeCurrency(undefined);
    const screen = render(<Exchange />);
    const element = await screen.queryByText("exchange");
    expect(element).not.toBeInTheDocument();
  });

  it("should apply correct page title", () => {
    rootStore.userStore.setCurrency("PLN");
    rootStore.userStore.setExchangeCurrency("USD");
    const screen = render(<Exchange />);
    expect(screen.getByText("exchange")).toBeInTheDocument();
    expect(document.title).toContain("Exchange currency");
  });
});
