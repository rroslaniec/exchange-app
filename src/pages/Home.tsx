import { useEffect } from "react";
import ExchangeLog from "../components/ExchangeLog";
import UserCurrencies from "../components/UserCurrencies";
import useUserStore from "../hooks/useUserStore";

const Home = () => {
  const wallet = useUserStore((store) => store.wallet);
  const exchangeLog = useUserStore((store) => store.exchangeLog);

  useEffect(() => {
    document.title = "Wallet | Exchange App";
  }, []);

  return (
    <>
      <h1>Your wallet</h1>
      <UserCurrencies wallet={wallet} />
      <ExchangeLog log={exchangeLog} />
    </>
  );
};

export default Home;
