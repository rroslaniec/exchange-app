import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { Redirect } from "react-router-dom";
import Button from "../components/Button";
import CurrencySelect from "../components/CurrencySelect";
import ExchangeBox from "../components/ExchangeBox";
import RateInfo from "../components/RateInfo";
import useUserStore from "../hooks/useUserStore";

const Exchange = () => {
  const { exchangeFrom, exchangeTo, updateWallet, hasValue } = useUserStore(
    (store) => ({
      exchangeFrom: store.exchangeFrom,
      exchangeTo: store.exchangeTo,
      hasValue: !!store.exchangeValue,
      updateWallet: store.updateWallet,
    })
  );

  useEffect(() => {
    document.title = "Exchange currency | Exchange App";
  }, []);

  if (!exchangeTo || !exchangeFrom) return <Redirect to="/" />;

  return (
    <>
      <div>
        <CurrencySelect />
        <RateInfo currency={exchangeFrom} />
      </div>
      <div>
        <ExchangeBox currency={exchangeFrom} />
        <ExchangeBox currency={exchangeTo} prefix="+" />
      </div>
      <Button type="button" onClick={() => updateWallet()} disabled={!hasValue}>
        exchange
      </Button>
    </>
  );
};

export default observer(Exchange);
