import { render, RenderOptions } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import { THEME } from "./config/theme";
import { GlobalStoreProvider } from "./contexts/GlobalContext";

const Providers: React.FC = ({ children }) => {
  return (
    <MemoryRouter>
      <GlobalStoreProvider>
        <ThemeProvider theme={THEME}>{children}</ThemeProvider>
      </GlobalStoreProvider>
    </MemoryRouter>
  );
};

const customRender = (ui: React.ReactElement, options?: RenderOptions) =>
  render(ui, { wrapper: Providers, ...options });

export * from "@testing-library/react";

export { customRender as render };
