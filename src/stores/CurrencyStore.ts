import {
  flow,
  makeAutoObservable,
  onBecomeUnobserved,
  computed,
  reaction,
} from "mobx";
import API from "../api";
import { PRECISION, RATES_UPDATE_TIME } from "../config/consts";
import { roundToPrecision } from "../utils/general";
import type { TRootStore } from "./index";

class CurrencyStore {
  currencies: CurrenciesResponse = {};

  fromRates: ExchangeRate = {};

  toRates: ExchangeRate = {};

  private rootStore: TRootStore;

  private interval: number | undefined;

  constructor(rootStore: TRootStore) {
    this.rootStore = rootStore;
    makeAutoObservable(this, {
      fetchCurrencies: flow,
      fetchRates: flow,
      currenciesWithRates: computed.struct,
      currentExchangeValue: computed.struct,
    });

    this.fetchCurrencies();

    /**
     * Side effect to refetch rates, apply interval and clear user value
     */
    reaction(
      () => [rootStore.userStore.exchangeFrom, rootStore.userStore.exchangeTo],
      () => {
        this.rootStore.userStore.exchangeValue = undefined;
        this.suspend();
        this.resume();
      }
    );
    onBecomeUnobserved(rootStore.userStore, "exchangeValue", this.suspend);
  }

  private resume = () => {
    this.interval = window.setInterval(
      () => this.fetchRates(),
      RATES_UPDATE_TIME
    );
    this.fetchRates();
  };

  private suspend = () => {
    window.clearInterval(this.interval);
  };

  *fetchCurrencies() {
    try {
      const currencies: CurrenciesResponse =
        yield API.Currencies.fetchCurrencies();
      this.currencies = currencies;
    } catch (erorr) {
      console.error(erorr);
    }
  }

  *fetchRates() {
    if (
      !this.rootStore.userStore.exchangeFrom ||
      !this.rootStore.userStore.exchangeTo
    ) {
      return;
    }
    try {
      const fromRatesResponse: RatesResponse = yield API.Rates.fetchRates(
        this.rootStore.userStore.exchangeFrom
      );
      const toRatesResponse: RatesResponse = yield API.Rates.fetchRates(
        this.rootStore.userStore.exchangeTo
      );
      this.fromRates = fromRatesResponse.rates;
      this.toRates = toRatesResponse.rates;
    } catch (erorr) {
      console.error(erorr);
    }
  }

  get currenciesWithRates() {
    const rateKeys = Object.keys(this.fromRates);
    return Object.keys(this.currencies).reduce((acc, key) => {
      if (!rateKeys.includes(key)) {
        return acc;
      }
      acc[key] = this.currencies[key];
      return acc;
    }, {} as CurrenciesResponse);
  }

  /**
   * Protects exchange process from amount of money lower than round to expected PRECISION
   * eg. PRECISION === 2, then guards values from < 0,01
   *
   */
  get currentExchangeValue() {
    if (
      !this.rootStore.userStore.exchangeTo ||
      !this.rootStore.userStore.exchangeFrom ||
      !this.fromRates[this.rootStore.userStore.exchangeTo] ||
      !this.toRates[this.rootStore.userStore.exchangeFrom] ||
      !this.rootStore.userStore.safeExchangeValue
    ) {
      return null;
    }
    const to =
      this.fromRates[this.rootStore.userStore.exchangeTo] *
      this.rootStore.userStore.safeExchangeValue;

    const displayFrom = roundToPrecision(
      this.rootStore.userStore.safeExchangeValue,
      PRECISION
    );
    const displayTo = roundToPrecision(to, PRECISION);
    if (!displayFrom || !displayTo) {
      return null;
    }
    if (displayTo <= 0 || displayFrom <= 0) {
      return null;
    }

    return {
      from: {
        code: this.rootStore.userStore.exchangeFrom,
        value: this.rootStore.userStore.safeExchangeValue,
      },
      to: {
        code: this.rootStore.userStore.exchangeTo,
        value: to,
      },
    };
  }

  /**
   * Calculates power for case with big differences between rates according to current PRECISION
   */
  calculatePrecisionPow = (value: number, rate: number) => {
    let minValue = rate * value * 10 ** PRECISION;
    let pow = 0;
    while (minValue < 1) {
      pow += 1;
      minValue = rate * value * 10 ** (PRECISION + pow);
    }
    return pow;
  };

  isCodeInCurrencies(code: string) {
    const keys = Object.keys(this.currencies);
    return keys.includes(code);
  }
}

export default CurrencyStore;
