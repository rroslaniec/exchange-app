/* eslint-disable @typescript-eslint/ban-ts-comment */
import { configure, observable, toJS, when } from "mobx";
import CurrencyStore from "./CurrencyStore";

// eslint-disable-next-line jest/no-mocks-import
import currenciesWithRates from "../api/__mocks__/currenciesWithRates.json";
import * as CONSTS from "../config/consts";

describe("CurrencyStore", () => {
  const fakeRootStore: any = observable({
    userStore: {
      exchangeValue: 10,
      exchangeFrom: undefined,
      exchangeTo: undefined,
      safeExchangeValue: 10,
    },
  });

  const store = new CurrencyStore(fakeRootStore);
  beforeAll(() => {
    configure({ enforceActions: "never" });
  });
  afterAll(() => {
    configure({ enforceActions: undefined });
  });

  it("should fetch currencies when creates instance", () => {
    expect(store.currencies.PLN).toBeDefined();
  });

  it("should fetch rates when exchangeFrom or exchangeTo changes", async () => {
    fakeRootStore.userStore.exchangeTo = "PLN";
    fakeRootStore.userStore.exchangeFrom = "PLN";
    await when(() => store.fromRates.PLN !== undefined);
    expect(Object.keys(toJS(store.fromRates)).length).toBeGreaterThan(0);
    expect(Object.keys(toJS(store.toRates)).length).toBeGreaterThan(0);
  });
  it("should correcly only return currencies with rates", () => {
    fakeRootStore.userStore.exchangeFrom = "USD";
    fakeRootStore.userStore.exchangeTo = "PLN";
    expect(store.currenciesWithRates).toEqual(currenciesWithRates);
  });
  it("should apply interval to refetch rates", async () => {
    fakeRootStore.userStore.exchangeFrom = "USD";
    fakeRootStore.userStore.exchangeTo = "PLN";
    // @ts-ignore
    expect(store.interval).toBeDefined();
  });
  it("should correctly calculate power", async () => {
    Object.defineProperty(CONSTS, "PRECISION", { value: 3 });
    expect(store.calculatePrecisionPow(0.1, 0.1)).toBe(0);
    expect(store.calculatePrecisionPow(0.1, 0.001)).toBe(1);
    expect(store.calculatePrecisionPow(1, 0.00001)).toBe(2);
    Object.defineProperty(CONSTS, "PRECISION", { value: 1 });
    expect(store.calculatePrecisionPow(1, 0.00001)).toBe(4);
  });
  /* eslint-disable prettier/prettier */
  it.each`
    input                                                                      | output
    ${{ exchangeTo: "PLN", exchangeFrom: "USD", safeExchangeValue: 10 }}       | ${{ from: { code: "USD", value: 10 }, to: { code: "PLN", value: 37.299348346 } }}
    ${{ exchangeTo: "PLN", exchangeFrom: "USD", safeExchangeValue: 0 }}        | ${null}
    ${{ exchangeTo: "PLN", exchangeFrom: "USD", safeExchangeValue: -1 }}       | ${null}
    ${{ exchangeTo: undefined, exchangeFrom: "USD", safeExchangeValue: 10 }}   | ${null}
    ${{ exchangeTo: "PLN", exchangeFrom: undefined, safeExchangeValue: 10 }}   | ${null}
    ${{ exchangeTo: "PLN", exchangeFrom: "undefined", safeExchangeValue: 10 }} | ${null}
    ${{ exchangeTo: "undefined", exchangeFrom: "USD", safeExchangeValue: 10 }} | ${null}
  `(
    '"currentExchangeValue" for given $input should returns $output',
    async ({ input, output }) => {
      Object.defineProperty(CONSTS, "PRECISION", { value: 2 });
      fakeRootStore.userStore = { ...fakeRootStore.userStore, ...input };
      await when(() => store.fromRates.PLN !== undefined);
      expect(store.currentExchangeValue).toEqual(output);
    }
  );
  /* eslint-disable prettier/prettier */
  it.each`
    input          | output
    ${"PLN"}       | ${true}
    ${"USD"}       | ${true}
    ${"foo"}       | ${false}
    ${"undefined"} | ${false}
    ${undefined}   | ${false}
  `(
    '"isCodeInCurrencies" for given $input should returns $output',
    async ({ input, output }) => {
      Object.defineProperty(CONSTS, "PRECISION", { value: 2 });
      fakeRootStore.userStore.exchangeFrom = "USD";
      fakeRootStore.userStore.exchangeTo = "PLN";
      await when(() => store.fromRates.PLN !== undefined);
      expect(store.isCodeInCurrencies(input)).toEqual(output);
    }
  );
});
