import { computed, makeAutoObservable, toJS } from "mobx";
import LocalStorage from "../utils/LocalStorage";
import { EXCHANGE_LOG_HISTORY, LocalStorageKeys } from "../config/consts";
import type { TRootStore } from "./index";

class UserStore {
  public exchangeFrom: string | undefined;

  public exchangeTo: string | undefined;

  public exchangeValue: number | undefined;

  public wallet: Wallet = new Map<string, number>();

  public exchangeLog: ExchangeLog[] = [];

  private rootStore: TRootStore;

  constructor(rootStore: TRootStore) {
    this.rootStore = rootStore;
    this.restoreWallet();

    makeAutoObservable(
      this,
      {
        safeExchangeValue: computed,
      },
      { autoBind: true }
    );
  }

  setCurrency(currency: string | undefined) {
    this.exchangeFrom = currency;
    if (currency === undefined) {
      this.exchangeTo = undefined;
    } else if (this.rootStore.currencyStore.isCodeInCurrencies(currency)) {
      this.exchangeTo =
        Array.from(this.wallet.keys())
          .filter((key) => key !== currency)
          .find(() => true) ||
        Object.keys(this.rootStore.currencyStore.currenciesWithRates)
          .filter((key) => key !== currency)
          .find(() => true);
    }

    this.exchangeValue = undefined;
  }

  setExchangeCurrency(exchangeTo: string | undefined) {
    if (exchangeTo === undefined || exchangeTo === this.exchangeFrom) {
      this.exchangeTo = undefined;
    } else if (this.rootStore.currencyStore.isCodeInCurrencies(exchangeTo)) {
      this.exchangeTo = exchangeTo;
    }
  }

  /**
   * Computed value to guard max possible exchange value.
   */
  get safeExchangeValue() {
    if (
      !this.exchangeFrom ||
      !this.wallet.has(this.exchangeFrom) ||
      !this.exchangeValue
    ) {
      return null;
    }
    const maxValue = this.wallet.get(this.exchangeFrom);
    return Math.max(
      this.exchangeValue > maxValue ? maxValue : this.exchangeValue,
      0
    );
  }

  private setExchangeValue(value: number) {
    if (!this.exchangeFrom || !this.exchangeTo) return;
    const maxValue = this.wallet.get(this.exchangeFrom) || 0;
    const rate = this.rootStore.currencyStore.fromRates[this.exchangeTo];
    if (!rate) return;
    const pow = this.rootStore.currencyStore.calculatePrecisionPow(value, rate);
    const safeValue = value * 10 ** pow;
    this.exchangeValue = Math.max(
      safeValue > maxValue ? maxValue : safeValue,
      0
    );
  }

  private reverseExchangeValue(value: number) {
    if (!this.exchangeTo || !this.exchangeFrom) return;
    const rate = this.rootStore.currencyStore.fromRates[this.exchangeTo];
    if (!rate) return;
    const maxValue = this.wallet.get(this.exchangeFrom) || 0;
    const calculated = value / rate;
    this.exchangeValue = Math.max(
      calculated >= maxValue ? maxValue : calculated,
      0
    );
  }

  /**
   * Gets full data about currency and set function for manipulate preview values
   */
  getDataAboutCurrency(currencyCode?: string) {
    if (!currencyCode) return null;
    const { currentExchangeValue } = this.rootStore.currencyStore;
    if (currencyCode === this.exchangeFrom) {
      return {
        value: currentExchangeValue?.from.value,
        inWallet: this.wallet.get(currencyCode) || 0,
        set: this.setExchangeValue,
      };
    }
    if (currencyCode === this.exchangeTo) {
      return {
        value: currentExchangeValue?.to.value,
        inWallet: this.wallet.get(currencyCode) || 0,
        set: this.reverseExchangeValue,
      };
    }
    return null;
  }

  /**
   * Recalculates wallet according to rates.
   * Based on actual store, not input.
   */
  updateWallet(): void {
    if (
      !this.rootStore.currencyStore.currentExchangeValue ||
      !this.exchangeValue
    ) {
      return;
    }
    const { from, to } = this.rootStore.currencyStore.currentExchangeValue;
    const fromValue = (this.wallet.get(from.code) || 0) - from.value;
    const toValue = (this.wallet.get(to.code) || 0) + to.value;
    if (from.code === to.code) {
      return;
    }
    if (fromValue < 0 || toValue < 0) {
      return;
    }
    this.wallet.set(from.code, fromValue);
    this.wallet.set(to.code, toValue);
    this.exchangeValue = 0;

    this.saveToLog({ from, to, date: new Date().toISOString() });
  }

  /**
   * Saves exchange log into LocalStorage
   * @param log Log item structure
   */
  private saveToLog(log: ExchangeLog) {
    this.exchangeLog.unshift(log);
    LocalStorage.saveJSON(
      LocalStorageKeys.WALLET,
      Object.fromEntries(toJS(this.wallet))
    );
    LocalStorage.saveJSON(
      LocalStorageKeys.EXCHANGE_LOG,
      toJS(this.exchangeLog).slice(0, EXCHANGE_LOG_HISTORY)
    );
  }

  private restoreWallet() {
    if (LocalStorage.hasItem(LocalStorageKeys.WALLET)) {
      const localStorageWallet = LocalStorage.getJSON(LocalStorageKeys.WALLET);
      this.wallet = new Map(Object.entries(localStorageWallet));
      this.exchangeLog =
        LocalStorage.getJSON(LocalStorageKeys.EXCHANGE_LOG) || [];
    } else {
      // default wallet
      this.wallet.set("EUR", 100);
      this.wallet.set("USD", 100);
    }
  }

  getRateInfoByCurrency(currency: string): {
    baseCurrency: string | undefined;
    ratesName: "toRates" | "fromRates";
  } | null {
    if (this.exchangeTo === this.exchangeFrom) {
      return null;
    }
    if (currency === this.exchangeTo) {
      return { baseCurrency: this.exchangeFrom, ratesName: "toRates" };
    }
    if (currency === this.exchangeFrom) {
      return { baseCurrency: this.exchangeTo, ratesName: "fromRates" };
    }
    return null;
  }
}

export default UserStore;
