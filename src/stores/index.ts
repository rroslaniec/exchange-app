// eslint-disable-next-line import/no-cycle
import CurrencyStore from "./CurrencyStore";
// eslint-disable-next-line import/no-cycle
import UserStore from "./UserStore";

class RootStore {
  public currencyStore: CurrencyStore;

  public userStore: UserStore;

  constructor() {
    this.userStore = new UserStore(this);
    this.currencyStore = new CurrencyStore(this);
  }
}

const rootStore = new RootStore();
export type TRootStore = typeof rootStore;

export default rootStore;
