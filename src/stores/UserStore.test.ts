/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable no-new */
import { configure, observable } from "mobx";
import LocalStorage from "../utils/LocalStorage";
import UserStore from "./UserStore";

describe("UserStore", () => {
  const fakeRootStore: any = observable({
    currencyStore: {
      exchangeValue: 10,
      exchangeFrom: undefined,
      exchangeTo: undefined,
      safeExchangeValue: 10,
      isCodeInCurrencies: () => true,
      currenciesWithRates: { FOO: 1 },
    },
  });

  beforeAll(() => {
    configure({ enforceActions: "never" });
    jest
      .useFakeTimers("modern")
      .setSystemTime(new Date("1990-01-01").getTime());
  });
  afterAll(() => {
    configure({ enforceActions: undefined });
    jest.useRealTimers();
  });

  it("should apply defualt wallet on init", () => {
    const store = new UserStore(fakeRootStore);
    expect(Array.from(store.wallet.keys()).length).toBeGreaterThan(1);
  });

  it("should apply data from LocalStorage", () => {
    const spyHasItem = jest.spyOn(LocalStorage, "hasItem");
    const spyGetJSON = jest.spyOn(LocalStorage, "getJSON");
    spyHasItem.mockReturnValueOnce(true);
    spyGetJSON.mockReturnValueOnce({ PLN: 10 });
    spyGetJSON.mockReturnValueOnce([
      {
        from: { code: "PLN", value: 1 },
        to: { code: "PLN", value: 1 },
        date: "test",
      },
    ]);
    const store = new UserStore(fakeRootStore);
    expect(spyHasItem).toBeCalled();
    expect(store.wallet.has("PLN")).toBeTruthy();
    expect(store.exchangeLog.length).toBe(1);
    spyHasItem.mockRestore();
    spyGetJSON.mockRestore();
  });

  /* eslint-disable prettier/prettier */
  it.each`
    exchangeFrom | exchangeTo | input    | output
    ${"PLN"}     | ${"USD"}   | ${"PLN"} | ${{ baseCurrency: "USD", ratesName: "fromRates" }}
    ${"PLN"}     | ${"USD"}   | ${"USD"} | ${{ baseCurrency: "PLN", ratesName: "toRates" }}
    ${"USD"}     | ${"USD"}   | ${"USD"} | ${null}
    ${"PLN"}     | ${"USD"}   | ${"EUR"} | ${null}
  `(
    '"getRateInfoByCurrency" for store values: $exchangeFrom, $exchangeTo and given data: $input should returns $output',
    ({ exchangeFrom, exchangeTo, input, output }) => {
      const store = new UserStore(fakeRootStore);
      /* eslint-enable prettier/prettier */
      store.exchangeFrom = exchangeFrom;
      store.exchangeTo = exchangeTo;
      expect(store.getRateInfoByCurrency(input)).toEqual(output);
    }
  );
  it("should save data in LocalStorage when triggers updateWallet", () => {
    const spySaveJSON = jest.spyOn(LocalStorage, "saveJSON");
    const store = new UserStore(fakeRootStore);
    fakeRootStore.currencyStore.currentExchangeValue = {
      from: { code: "PLN", value: 10 },
      to: { code: "USD", value: 10 },
    };
    store.exchangeValue = 10;
    store.exchangeFrom = "PLN";
    store.exchangeTo = "USD";
    store.wallet.set("PLN", 10);
    store.updateWallet();
    expect(spySaveJSON).toHaveBeenNthCalledWith(1, "WALLET", {
      EUR: 100,
      PLN: 0,
      USD: 110,
    });
    expect(spySaveJSON).toHaveBeenNthCalledWith(2, "EXCHANGE_LOG", [
      {
        date: "1990-01-01T00:00:00.000Z",
        from: { code: "PLN", value: 10 },
        to: { code: "USD", value: 10 },
      },
    ]);
    spySaveJSON.mockRestore();
  });
  it("should clear value after update wallet", () => {
    const store = new UserStore(fakeRootStore);
    fakeRootStore.currencyStore.currentExchangeValue = {
      from: { code: "PLN", value: 10 },
      to: { code: "USD", value: 10 },
    };
    store.exchangeValue = 10;
    store.exchangeFrom = "PLN";
    store.exchangeTo = "USD";
    store.wallet.set("PLN", 10);
    store.updateWallet();
    expect(store.exchangeValue).toBe(0);
  });

  it("should guards negative exchange to happens", () => {
    const store = new UserStore(fakeRootStore);
    fakeRootStore.currencyStore.currentExchangeValue = {
      from: { code: "EUR", value: 10 },
      to: { code: "USD", value: 10 },
    };
    store.exchangeValue = 10;
    store.exchangeFrom = "PLN";
    store.exchangeTo = "USD";
    store.wallet.set("USD", 0);
    store.wallet.set("EUR", 0);
    store.updateWallet();
    expect(store.exchangeValue).toBe(10);
    expect(store.wallet.get("USD")).toBe(0);
    expect(store.wallet.get("EUR")).toBe(0);
  });
  it("should guards to do not change same currency", () => {
    const store = new UserStore(fakeRootStore);
    fakeRootStore.currencyStore.currentExchangeValue = {
      from: { code: "EUR", value: 10 },
      to: { code: "EUR", value: 10 },
    };
    store.exchangeValue = 10;
    store.exchangeFrom = "PLN";
    store.exchangeTo = "USD";
    store.wallet.set("USD", 100);
    store.wallet.set("EUR", 100);
    store.wallet.set("PLN", 100);
    store.updateWallet();
    expect(store.exchangeValue).toBe(10);
    expect(store.wallet.get("USD")).toBe(100);
    expect(store.wallet.get("EUR")).toBe(100);
  });
  /* eslint-disable prettier/prettier */
  it.each`
    input        | exchangeFrom | exchangeTo | wallet          | currentExchangeValue                                                    | output
    ${undefined} | ${"USD"}     | ${"PLN"}   | ${{ PLN: 10 }}  | ${{ from: { code: "EUR", value: 10 }, to: { code: "USD", value: 10 } }} | ${null}
    ${"EUR"}     | ${"USD"}     | ${"PLN"}   | ${{ PLN: 10 }}  | ${{ from: { code: "EUR", value: 10 }, to: { code: "USD", value: 10 } }} | ${null}
    ${"PLN"}     | ${"USD"}     | ${"PLN"}   | ${{ PLN: 100 }} | ${{ from: { code: "EUR", value: 10 }, to: { code: "USD", value: 10 } }} | ${null}
    ${"PLN"}     | ${"USD"}     | ${"PLN"}   | ${{ PLN: 100 }} | ${{ from: { code: "EUR", value: 10 }, to: { code: "PLN", value: 10 } }} | ${{ inWallet: 100, value: 10 }}
    ${"PLN"}     | ${"PLN"}     | ${"USD"}   | ${{ PLN: 100 }} | ${{ from: { code: "PLN", value: 10 }, to: { code: "USD", value: 10 } }} | ${{ inWallet: 100, value: 10 }}
    ${"PLN"}     | ${"PLN"}     | ${"USD"}   | ${{ PLN: 0 }}   | ${{ from: { code: "PLN", value: 10 }, to: { code: "USD", value: 10 } }} | ${{ inWallet: 0, value: 10 }}
  `(
    '"getDataAboutCurrency" for store data: $exchangeFrom, $exchangeTo, $wallet and given $input should returns $output',
    /* eslint-enable prettier/prettier */
    ({
      input,
      exchangeFrom,
      exchangeTo,
      wallet,
      currentExchangeValue,
      output,
    }) => {
      fakeRootStore.currencyStore = {
        ...fakeRootStore.currencyStore,
        currentExchangeValue,
      };
      const store = new UserStore(fakeRootStore);
      store.exchangeFrom = exchangeFrom;
      store.exchangeTo = exchangeTo;
      store.wallet = new Map(Object.entries(wallet));
      expect(store.getDataAboutCurrency(input)).toEqual(
        expect.objectContaining(output)
      );
    }
  );
  /* eslint-disable prettier/prettier */
  it.each`
    exchangeFrom   | exchangeValue | wallet         | output
    ${"PLN"}       | ${10}         | ${{ PLN: 10 }} | ${10}
    ${"PLN"}       | ${100}        | ${{ PLN: 10 }} | ${10}
    ${"undefined"} | ${9}          | ${{ PLN: 10 }} | ${null}
    ${undefined}   | ${9}          | ${{ PLN: 10 }} | ${null}
    ${"PLN"}       | ${9}          | ${{ PLN: 0 }}  | ${0}
    ${"PLN"}       | ${9}          | ${{ PLN: -1 }} | ${0}
    ${"PLN"}       | ${-1}         | ${{ PLN: 10 }} | ${0}
  `(
    '"safeExchangeValue" for given state: $exchangeFrom, $exchangeValue, $wallet should return $output',
    ({ exchangeFrom, exchangeValue, wallet, output }) => {
      /* eslint-enable prettier/prettier */
      const store = new UserStore(fakeRootStore);
      store.exchangeValue = exchangeValue;
      store.exchangeFrom = exchangeFrom;
      store.wallet = new Map(Object.entries(wallet));
      expect(store.safeExchangeValue).toEqual(output);
    }
  );
  /* eslint-disable prettier/prettier */
  it.each`
    input        | wallet                  | exchangeTo
    ${"PLN"}     | ${{ PLN: 10 }}          | ${"FOO"}
    ${"USD"}     | ${{ PLN: 10, EUR: 10 }} | ${"PLN"}
    ${undefined} | ${{ PLN: 10, EUR: 10 }} | ${undefined}
  `(
    '"setCurrency" for given $input and $wallet shoulds set exchangeTo to $exchangeTo',
    ({ input, wallet, exchangeTo }) => {
      /* eslint-enable prettier/prettier */
      const store = new UserStore(fakeRootStore);
      store.wallet = new Map(Object.entries(wallet));
      store.exchangeValue = 10;
      store.setCurrency(input);
      expect(store.exchangeTo).toEqual(exchangeTo);
      expect(store.exchangeValue).toEqual(undefined);
    }
  );
  /* eslint-disable prettier/prettier */
  it.each`
    input    | isInCurrencies | exchangeFrom | exchangeTo
    ${"PLN"} | ${false}       | ${"USD"}     | ${undefined}
    ${"USD"} | ${true}        | ${"USD"}     | ${undefined}
  `(
    '"setExchangeCurrency" for given $input, $exchangeFrom and $isInCurrencies shoulds set exchangeTo to $exchangeTo',
    ({ input, isInCurrencies, exchangeFrom, exchangeTo }) => {
      /* eslint-enable prettier/prettier */
      fakeRootStore.currencyStore = {
        ...fakeRootStore.currencyStore,
        isCodeInCurrencies: () => isInCurrencies,
      };
      const store = new UserStore(fakeRootStore);
      store.exchangeFrom = exchangeFrom;
      store.setExchangeCurrency(input);
      expect(store.exchangeTo).toEqual(exchangeTo);
    }
  );

  /* eslint-disable prettier/prettier */
  it.each`
    input  | exchangeTo | exchangeFrom | wallet            | rate    | pow  | output
    ${1}   | ${false}   | ${"USD"}     | ${{ PLN: 10 }}    | ${0.12} | ${1} | ${undefined}
    ${1}   | ${"PLN"}   | ${undefined} | ${{ PLN: 10 }}    | ${0.12} | ${1} | ${undefined}
    ${1}   | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }}    | ${0.12} | ${0} | ${1}
    ${1.1} | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }}    | ${0.12} | ${0} | ${1.1}
    ${1.1} | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }}    | ${0.12} | ${3} | ${10}
    ${11}  | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10000 }} | ${0.12} | ${2} | ${1100}
    ${11}  | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 0 }}     | ${0.12} | ${2} | ${0}
    ${11}  | ${"PLN"}   | ${"PLN"}     | ${{ PLN: -1 }}    | ${0.12} | ${2} | ${0}
    ${-1}  | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }}    | ${0.12} | ${2} | ${0}
    ${-1}  | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }}    | ${null} | ${2} | ${undefined}
  `(
    '"setExchangeValue" for $input and store should apply exchangeValue: $output',
    /* eslint-enable prettier/prettier */
    ({ input, rate, pow, output, exchangeFrom, exchangeTo, wallet }) => {
      fakeRootStore.currencyStore = {
        ...fakeRootStore.currencyStore,
        fromRates: {
          [exchangeTo]: rate,
        },
        calculatePrecisionPow: () => pow,
      };
      const store = new UserStore(fakeRootStore);
      store.exchangeValue = undefined;
      store.wallet = new Map(Object.entries(wallet));
      store.exchangeTo = exchangeTo;
      store.exchangeFrom = exchangeFrom;
      // @ts-ignore
      store.setExchangeValue(input);
      expect(store.exchangeValue).toEqual(output);
    }
  );
  // private reverseExchangeValue(value: number) {
  //   if (!this.exchangeTo || !this.exchangeFrom) return;
  //   const rate = this.rootStore.currencyStore.fromRates[this.exchangeTo];
  //   if (!rate) return;
  //   const maxValue = this.wallet.get(this.exchangeFrom) || 0;
  //   const calculated = value / rate;
  //   this.exchangeValue = Math.max(
  //     calculated >= maxValue ? maxValue : calculated,
  //     0
  //   );
  // }
  /* eslint-disable prettier/prettier */
  it.each`
    input   | exchangeTo | exchangeFrom | wallet         | rate    | output
    ${1}    | ${false}   | ${"USD"}     | ${{ PLN: 10 }} | ${0.12} | ${undefined}
    ${1}    | ${"PLN"}   | ${undefined} | ${{ PLN: 10 }} | ${0.12} | ${undefined}
    ${-1}   | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }} | ${null} | ${undefined}
    ${1}    | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }} | ${0.12} | ${1 / 0.12}
    ${1.1}  | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }} | ${0.12} | ${1.1 / 0.12}
    ${1000} | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 10 }} | ${1}    | ${10}
    ${-1}   | ${"PLN"}   | ${"PLN"}     | ${{ PLN: -1 }} | ${1}    | ${0}
    ${-1}   | ${"PLN"}   | ${"PLN"}     | ${{ PLN: 0 }}  | ${1}    | ${0}
  `(
    '"reverseExchangeValue" for $input and store should apply exchangeValue: $output',
    /* eslint-enable prettier/prettier */
    ({ input, rate, output, exchangeFrom, exchangeTo, wallet }) => {
      fakeRootStore.currencyStore = {
        ...fakeRootStore.currencyStore,
        fromRates: {
          [exchangeTo]: rate,
        },
      };
      const store = new UserStore(fakeRootStore);
      store.wallet = new Map(Object.entries(wallet));
      store.exchangeTo = exchangeTo;
      store.exchangeFrom = exchangeFrom;
      // @ts-ignore
      store.reverseExchangeValue(input);
      expect(store.exchangeValue).toEqual(output);
    }
  );
});
