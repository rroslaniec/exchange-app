import { Switch, Route } from "react-router-dom";
import Exchange from "./pages/Exchange";
import Home from "./pages/Home";

function App() {
  return (
    <Switch>
      <Route path="/exchange">
        <Exchange />
      </Route>
      <Route path="/">
        <Home />
      </Route>
    </Switch>
  );
}

export default App;
