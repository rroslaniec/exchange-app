// credits: https://github.com/microsoft/TypeScript/issues/13086#issuecomment-269695308
interface Map<K, V> {
  has<KnownKeys extends string, CheckedString extends string>(
    this: MapWith<string, V, KnownKeys>,
    key: CheckedString
  ): this is MapWith<K, V, CheckedString | KnownKeys>;

  has<CheckedString extends string>(
    this: Map<string, V>,
    key: CheckedString
  ): this is MapWith<K, V, CheckedString>;
}
