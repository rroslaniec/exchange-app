interface MapWith<K, V, DefiniteKey extends K> extends Map<K, V> {
  get(k: DefiniteKey): V;
  get(k: K): V | undefined;
}

interface Currency {
  code: string;
  name: string;
}

interface ExchangeLog {
  from: {
    code: string;
    value: number;
  };
  to: {
    code: string;
    value: number;
  };
  date: string;
}

type ExchangePrefix = "+" | "-";

type Wallet = Map<string, number>;
