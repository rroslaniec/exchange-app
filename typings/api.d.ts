interface CurrenciesResponse {
  [key: string]: string;
}

interface ErrorResponse {
  status: number;
  error?: string;
}

interface ExchangeRate {
  [key: string]: number;
}

interface RatesResponse {
  base: string;
  rates: ExchangeRate;
  date: string;
}
